class RTC{
    constructor(ws_ep){
        this.signaling=new WebSocket(ws_ep);
        this.signaling.onmessage=e=>{
            const json=JSON.parse(e.data);
            if(json==null){
                this.makeOffer();
            }else{
                if(json.type=="answer"){
                    this.receiveAnswer(new RTCSessionDescription(json));
                }else{
                    this.receiveOffer(new RTCSessionDescription(json));
                }
            }
        }
        this.signaling.onclose=()=>{
            if(this.con.iceConnectionState!="connected"){
                alert("接続に失敗しました");
            }
        }
        this.con=new RTCPeerConnection({
            "iceServers":[
                {"url":"stun://stun2.l.google.com:19302"}
            ]
        });
        this.dataChannel=this.con.createDataChannel("Text");
        this.dataChannel.addEventListener("message",e=>{
            this.onmessage(e);
        });

        this.con.onicecandidate=e=>{
            if(!e.candidate){
                this.sendSDP(this.con.localDescription);
            }
        }
        this.con.onconnectionstatechange=e=>{
            switch(this.con.connectionState){
                case "connected":
                    this.signaling.close();
                    break;
                case "closed":
                    this.onclose();
                    break;
            }
        }
        this.con.onmessage=this.onmessage;
    }
    sendSDP(desc){
        this.signaling.send(JSON.stringify(desc));
    }
    send(value){
        this.dataChannel.send(value);
    }
    makeOffer(){
        this.con.createOffer().then(sessionDesc=>{
            return this.con.setLocalDescription(sessionDesc);
        }).catch(e=>{
            console.error(e);
        });
    }
    receiveOffer(sessionDesc){
        this.con.setRemoteDescription(sessionDesc).then(()=>{
            this.makeAnswer();
        }).catch(e=>{
            console.error(e);
        });
    }
    makeAnswer(){
        this.con.createAnswer().then(sessionDesc=>{
            this.con.setLocalDescription(sessionDesc);
            this.con.addEventListener("datachannel",e=>{
                this.dataChannel=e.channel;
                this.dataChannel.addEventListener("message",e=>{
                    this.onmessage(e);
                });
            });
        }).catch(e=>{
            console.error(e);
        });
    }
    receiveAnswer(sessionDesc){
        this.con.setRemoteDescription(sessionDesc).then(()=>{
            this.signaling.close();
        }).catch(e=>{
            console.error(e);
        });
    }    
}
