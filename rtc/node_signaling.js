const server=require("ws").Server;
const s=new server({port:2000});

var table="null";
s.on("connection",ws=>{
    ws.send(table);
    ws.on("message",message=>{
        console.log(message);
        if((JSON.parse(message)).type=="offer"){
            table=message;
        }
        s.clients.forEach(c=>{
            if(c!==ws) c.send(message);
        });
    });
});
