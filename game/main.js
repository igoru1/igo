/**
 * 画面ロード完了後にすべての処理を実行する
 */
window.onload=function(){
    /* 一辺640ピクセルの19路盤を作成 */
    var goban=new Goban(document.querySelector("#board"),640,19);
    goban.chmode(2);
    /* 自分の石は黒に設定 */
    var me=0;
    /* 変数turnを1づつ足し、それが偶数なら自分のターン */
    var turn=0;
    var ko=[null,null];
    goban.onclickpoint=function(e){
        var ban=goban.getBoard();
        var x=e.point[0];
        var y=e.point[1];
        if(ban[y][x]==undefined||ban[y][x]==null){
            ban[y][x]=turn%2;
        }else{
            return false;
        }
        var stealable=[];
        if(x>0&&!checkBreath(createCheckMap(ban),ban,1-(turn%2),x-1,y)){
            stealable=getStealable(createCheckMap(ban),ban,x-1,y);
        }else if(x<ban.length-1&&!checkBreath(createCheckMap(ban),ban,1-(turn%2),x+1,y)){
            stealable=getStealable(createCheckMap(ban),ban,x+1,y);
        }else if(y>0&&!checkBreath(createCheckMap(ban),ban,1-(turn%2),x,y-1)){
            stealable=getStealable(createCheckMap(ban),ban,x,y-1);
        }else if(y<ban.length-1&&!checkBreath(createCheckMap(ban),ban,1-(turn%2),x,y+1)){
            stealable=getStealable(createCheckMap(ban),ban,x,y+1);
        }
        if(ko[0]==x&&ko[1]==y){
            return false;
        }
        ko=kojudge(ban,stealable,x,y);
        console.log(ko);
        stealable.forEach(function(point){
            ban[point[1]][point[0]]=null;
        });
        if(!checkBreath(createCheckMap(ban),ban,(turn%2),x,y)){
            return false;
        }
        /*else if(ko[0]==x&&ko[1]==y){
            return false;
        }*/
        goban.setBoard(ban);
        /*if(turn%2==me){
            goban.plot(x,y,me);
            //turn++;
        }else{
            goban.plot(x,y,1-me);
        }*/
        turn++;
    };

}

/**
 * 合法手の判定
 */
function isLegal(ban,color,x,y){
    if(ban[y][x]!=null){
        return false;
    }
    var checkmap=createCheckMap(ban);
    if(checkBreath(checkmap,ban,color,x,y)==false){
        return false
    }
    return true;
}
/**
 * 呼吸点があるか確認
 * あればtrue なければfalseを返す
 */
function checkBreath(checkmap,ban,color,x,y){
    if(ban[y][x]==null||ban[y][x]==undefined){
        return true;
    }else{
        var flg=false;
        checkmap[y][x]=1;
        if(x>0){
            if(ban[y][x-1]==null){
                return true;
            }else if(ban[y][x-1]==color&&checkmap[y][x-1]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x-1,y);
            }
        }
        if(x<ban.length-1){
            if(ban[y][x+1]==null){
                return true;
            }else if(ban[y][x+1]==color&&checkmap[y][x+1]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x+1,y);
            }
        }
        if(y>0){
            if(ban[y-1][x]==null){
                return true;
            }else if(ban[y-1][x]==color&&checkmap[y-1][x]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x,y-1);
            }
        }
        if(y<ban.length-1){
            if(ban[y+1][x]==null){
                return true;
            }else if(ban[y+1][x]==color&&checkmap[y+1][x]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x,y+1);
            }
        }
        return flg;
    }
}

function getStealable(checkmap,ban,x,y){
    var result=new Array();
    checkmap[y][x]=1;
    result.push([x,y]);
    var color=ban[y][x];
    if(x>0){
        if(ban[y][x-1]==color&&checkmap[y][x-1]!=1){
            result=result.concat(getStealable(checkmap,ban,x-1,y));
        }
    }
    if(x<ban.length-1){
        if(ban[y][x+1]==color&&checkmap[y][x+1]!=1){
            result=result.concat(getStealable(checkmap,ban,x+1,y));
        }
    }
    if(y>0){
        if(ban[y-1][x]==color&&checkmap[y-1][x]!=1){
            result=result.concat(getStealable(checkmap,ban,x,y-1));
        }
    }
    if(y<ban.length-1){
        if(ban[y+1][x]==color&&checkmap[y+1][x]!=1){
            result=result.concat(getStealable(checkmap,ban,x,y+1));
        }
    }
    return result;
}

/**
 * コウ
 *石を取った後その取った石がアタリの場合でも相手はすぐに取り返せない。一度他のところに打ってから取り返す。
 */

function kojudge(ban,stealable,x,y){
    if(stealable.length==1&&!checkBreath(createCheckMap(ban),ban,ban[y][x],x,y)){
        return stealable[0];
    }else{
        return [null,null];
    }
}


    /**
     * 終局
　　　*投了したら終局
     *パスして相手がパスしなかったら続行、相手もパスだったら終局
    /

    var flg=0;
    flg +=1(flg=++;)
    flg =0;
    flg ==2;

    
     * 勝敗
     *黒が多ければ黒の勝ち、黒が少なければ白の勝ち、同数なら引き分け
     */







function createCheckMap(ban){
    var s=ban.length;
    var map=new Array(s);
    for(var i=0;i<s;i++) map[i]=new Array(s);
    return map;
}
