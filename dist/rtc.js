class RTC{
    constructor(roomid,me){
        this.db=firebase.database();
        this.role=null;
        this.room=this.db.ref("rooms/"+roomid);
        this.a_sdp_table=this.room.child("answer");
        this.o_sdp_table=this.room.child("offer");
        this.room.child("color").on("value",e=>{
            this.hostcolor=e.val();
        });
        this.room.child("size").on("value",e=>{
            this.size=e.val();
        });
        this.o_sdp_table.on("value",e=>{
            const json=JSON.parse(e.val());
            if(json==null){
                this.makeOffer();
                this.role="offerer";
            }else if(this.role==null){
                this.role="answerer";
                this.receiveOffer(new RTCSessionDescription(json));
            }
        });
        this.a_sdp_table.on("value",e=>{
            const json=JSON.parse(e.val());
            if(json==null) return false;
            if(this.role=="offerer"){
                this.receiveAnswer(new RTCSessionDescription(json));
            }
        });
        this.con=new RTCPeerConnection({
            "iceServers":[
                {"urls":"stun://stun2.l.google.com:19302"}
            ]
        });
        this.dataChannel=this.con.createDataChannel("Text");
        this.dataChannel.addEventListener("message",e=>{
            this.onmessage(JSON.parse(e.data));
        });

        this.con.onicecandidate=e=>{
            if(!e.candidate){
                this.sendSDP(this.con.localDescription);
            }
        }
        this.con.onconnectionstatechange=async e=>{
            switch(this.con.connectionState){
                case "connected":
                    console.log("opened!")
                    if(this.dataChannel.readyState!="open"){
                        await (()=>{
                            return new Promise((resolve,reject)=>{
                                this.dataChannel.onopen=()=>{
                                    resolve();
                                }
                            });
                        })();
                    }
                    if(this.role=="offerer") this.send({type:"prepare",size:this.size,color:this.hostcolor>=0?this.hostcolor:Math.round(Math.random()),ok:false});
                    this.room.remove();
                    this.db.goOffline();
                    break;
                case "closed":
                    this.onclose();
                    break;
            }
        }
        window.onbeforeunload=()=>{
            this.room.remove();
            this.db.goOffline();
        }
        //this.con.onmessage=this.onmessage;
    }
    sendSDP(desc){
        if(desc.type=="answer"){
            this.a_sdp_table.set(JSON.stringify(desc));
        }else{
            this.o_sdp_table.set(JSON.stringify(desc));
        }
    }
    send(value){
        this.dataChannel.send(JSON.stringify(value));
    }
    makeOffer(){
        console.log("m o!")
        this.con.createOffer().then(sessionDesc=>{
            return this.con.setLocalDescription(sessionDesc);
        }).catch(e=>{
            console.error(e);
        });
    }
    receiveOffer(sessionDesc){
        console.log("r o!")
        this.con.setRemoteDescription(sessionDesc).then(()=>{
            this.makeAnswer();
        }).catch(e=>{
            console.error(e);
        });
    }
    makeAnswer(){
        console.log("m a!")
        this.con.createAnswer().then(sessionDesc=>{
            this.con.setLocalDescription(sessionDesc);
            this.con.addEventListener("datachannel",e=>{
                this.dataChannel=e.channel;
                this.dataChannel.addEventListener("message",e=>{
                    this.onmessage(JSON.parse(e.data));
                });
            });
        }).catch(e=>{
            console.error(e);
        });
    }
    receiveAnswer(sessionDesc){
        console.log("r a!")
        this.con.setRemoteDescription(sessionDesc).catch(e=>{
            console.error(e);
        });
    }
}
