/**
 * 画面ロード完了後にすべての処理を実行する
 */
window.onload=async function(){
    var goban;
    var me;
    var turn=0;
    var ko=[null,null];
    var rtc;
    var roomid=(new URL(location.href)).searchParams.get("room_id");
    var user=await getSession();
    var age=[0,0];
    var endFlg=false;
    rtc=new RTC(roomid,user.uid);
    rtc.onmessage=async (e)=>{
        if(e.type=="prepare"){
            var rect=document.querySelector("#stage").getBoundingClientRect()
            me=1-e.color;
            document.querySelector("#myname").innerHTML=me==0?"黒":"白";
            document.querySelector("#opname").innerHTML=me==0?"白":"黒";
            goban=new Goban(document.querySelector("#board"),rect.width,e.size);
            goban.chmode(2);
            goban.onsetpoint=function(e){
                if(turn%2==1-me) return false;
                this.setWaiting(turn%2,e.point[0],e.point[1]);
            }
            goban.onmovepoint=function(e){
                if(turn%2==1-me) return false;
                this.setWaiting(turn%2,e.point[0],e.point[1]);
            }
            goban.onclickpoint=function(e){
                if(turn%2==1-me) return false;
                var ban=goban.getBoard();
                var x=e.point[0];
                var y=e.point[1];
                if(ban[y][x]==undefined||ban[y][x]==null){
                    ban[y][x]=me;
                }else{
                    return false;
                }
                var stealable=[];
                if(x>0&&!checkBreath(createCheckMap(ban),ban,1-me,x-1,y)){
                    stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x-1,y));
                }
                if(x<ban.length-1&&!checkBreath(createCheckMap(ban),ban,1-me,x+1,y)){
                    stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x+1,y));
                }
                if(y>0&&!checkBreath(createCheckMap(ban),ban,1-me,x,y-1)){
                    stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x,y-1));
                }
                if(y<ban.length-1&&!checkBreath(createCheckMap(ban),ban,1-me,x,y+1)){
                    stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x,y+1));
                }
                if(ko[0]==x&&ko[1]==y) return false;
                ko=kojudge(ban,stealable,x,y);
                stealable.forEach(function(point){
                    age[me]++;
                    document.querySelector("#myage").innerHTML=age[me];
                    ban[point[1]][point[0]]=null;
                });
                if(checkBreath(createCheckMap(ban),ban,me,x,y)==false&&stealable.length==0){
                    return false;
                }
                turn++;
                rtc.send({type:"point",data:[x,y]});
                goban.setBoard(ban);
            };
            switch(Math.floor(5-5*(await eval(ban,x,y))/361)){
                case 0:
                case 1:
                    break;
                case 2:
                    good();
                    break;
                case 3:
                    great();
                    break;
                case 4:
                    excellent();
                    break;
            }
            if(!e.ok) rtc.send({type:"prepare",size:e.size,color:me,ok:true});
            return false;
        }else if(e.type=="pass"){
            turn++;
            if(endFlg==true){
                var ban=goban.getBoard();
                var gi=endGame(ban);
                var result=[gi[0]-age[1],gi[1]-age[0]];
                endRoll(gi,age,result,me);
            }
            endFlg=!endFlg;
            return false;
        }else if(e.type=="lose"){
            endRollDeclared(me);
            return false;
        }
        var ban=goban.getBoard();
        var x=e.data[0];
        var y=e.data[1];
        ban[y][x]=1-me;
        var stealable=[];
        if(x>0&&!checkBreath(createCheckMap(ban),ban,me,x-1,y)){
            stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x-1,y));
        }
        if(x<ban.length-1&&!checkBreath(createCheckMap(ban),ban,me,x+1,y)){
            stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x+1,y));
        }
        if(y>0&&!checkBreath(createCheckMap(ban),ban,me,x,y-1)){
            stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x,y-1));
        }
        if(y<ban.length-1&&!checkBreath(createCheckMap(ban),ban,me,x,y+1)){
            stealable=stealable.concat(getStealable(createCheckMap(ban),ban,x,y+1));
        }
        ko=kojudge(ban,stealable,x,y);
        stealable.forEach(function(point){
            ban[point[1]][point[0]]=null;
            age[1-me]++;
            document.querySelector("#opage").innerHTML=age[me];
        });
        turn++;

        goban.setBoard(ban);
    }
    document.querySelector("#pass").onclick=()=>{
        rtc.send({type:"pass"});
        if(endFlg==true){
            var ban=goban.getBoard();
            var gi=endGame(ban);
            var result=[gi[0]-age[1],gi[1]-age[0]];
            endRoll(gi,age,result,me);
        }
        endFlg=!endFlg;
    }
    document.querySelector("#lose").onclick=()=>{
        rtc.send({type:"lose"});
        endRollDeclare(me);
    }
}
function endRollDeclared(me){
    const dialog=document.querySelector("#end_stage");
    const winner=me==0?"黒":"白";
    dialog.innerHTML=`
        <h3>終局</h3>
        <p>相手が投了しました</p>
        <div class="big_echo">${winner}のかち</div>
        <button onclick="history.back()">もどる</button>
        `;
    document.querySelector("#end_dialog").showModal();

}
function endRollDeclare(me){
    const dialog=document.querySelector("#end_stage");
    const winner=me==0?"白":"黒";
    dialog.innerHTML=`
        <h3>終局</h3>
        <p>あなたが投了しました</p>
        <div class="big_echo">${winner}のかち</div>
        <button onclick="history.back()">もどる</button>
        `;
    document.querySelector("#end_dialog").showModal();

}

function endRoll(gi,age,result,me){
    const dialog=document.querySelector("#end_stage");
    dialog.innerHTML=`
        <h3>終局</h3>
        <table>
            <tr>
                <th></th>
                <th>黒</th>
                <th>白</th>
            </tr>
            <tr>
                <th>地の数</th>
                <td>${gi[0]}</td>
                <td>${gi[1]}</td>
            </tr>
            <tr>
                <th>とったアゲハマ</th>
                <td>${age[0]}</td>
                <td>${age[1]}</td>
            </tr>
            <tr>
                <th>合計</th>
                <td>${result[0]}</td>
                <td>${result[1]}</td>
            </tr>
        </table>
        <div class="big_echo">${winner}のかち</div>
        <button onclick="history.back()">もどる</button>
    `;
document.querySelector("#end_dialog").showModal();
}
function endGame(ban){
    var lines=[];
    var checkmap=createCheckMap(ban);
    for(var i=0;i<ban.length;i++){
        for(var j=0;j<ban.length;j++){
            if(checkmap[i][j]==1) continue;
            if(ban[i][j]!=null&&ban[i][j]!=undefined){
                lines.push(getLine(checkmap,ban,null,j,i));
            }
        }
    }
    var groups=getGroup(lines);
    var tree=getGroupTree(groups,ban.length);
    var w=0;
    var b=0;
    for(var i=0;i<ban.length;i++){
        for(var j=0;j<ban.length;j++){
            groups.some(g=>{
                if(!g.closed) return false;
                if(ban[j][i]!=null||ban[j][i]!=undefined) return false;
                var flg=isIn(g,i,j,ban.length)
                if(flg){
                    g.point++;
                    if(g.parent) g.parent.point--;
                }
            });
        }
    }
    tree.some(t=>{
        var points=t.getPoints();
        b+=points[0];
        w+=points[1];
    });
    return [b,w];
}

/**
 * 合法手の判定
 */
function isLegal(ban,color,x,y){
    if(ban[y][x]!=null){
        return false;
    }
    var checkmap=createCheckMap(ban);
    if(checkBreath(checkmap,ban,color,x,y)==false){
        return false
    }
    return true;
}
/**
 * 呼吸点があるか確認
 * あればtrue なければfalseを返す
 */
function checkBreath(checkmap,ban,color,x,y){
    if(color==1-ban[y][x]||ban[y][x]==null||ban[y][x]==undefined){
        return true;
    }else{
        var flg=false;
        checkmap[y][x]=1;
        if(x>0){
            if(ban[y][x-1]==null){
                return true;
            }else if(ban[y][x-1]==color&&checkmap[y][x-1]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x-1,y);
            }
        }
        if(x<ban.length-1){
            if(ban[y][x+1]==null){
                return true;
            }else if(ban[y][x+1]==color&&checkmap[y][x+1]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x+1,y);
            }
        }
        if(y>0){
            if(ban[y-1][x]==null){
                return true;
            }else if(ban[y-1][x]==color&&checkmap[y-1][x]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x,y-1);
            }
        }
        if(y<ban.length-1){
            if(ban[y+1][x]==null){
                return true;
            }else if(ban[y+1][x]==color&&checkmap[y+1][x]!=1){
                flg=flg||checkBreath(checkmap,ban,color,x,y+1);
            }
        }
        return flg;
    }
}

function getStealable(checkmap,ban,x,y){
    var result=new Array();
    checkmap[y][x]=1;
    result.push([x,y]);
    var color=ban[y][x];
    if(x>0){
        if(ban[y][x-1]==color&&checkmap[y][x-1]!=1){
            result=result.concat(getStealable(checkmap,ban,x-1,y));
        }
    }
    if(x<ban.length-1){
        if(ban[y][x+1]==color&&checkmap[y][x+1]!=1){
            result=result.concat(getStealable(checkmap,ban,x+1,y));
        }
    }
    if(y>0){
        if(ban[y-1][x]==color&&checkmap[y-1][x]!=1){
            result=result.concat(getStealable(checkmap,ban,x,y-1));
        }
    }
    if(y<ban.length-1){
        if(ban[y+1][x]==color&&checkmap[y+1][x]!=1){
            result=result.concat(getStealable(checkmap,ban,x,y+1));
        }
    }
    return result;
}

function createCheckMap(ban){
    var s=ban.length;
    var map=new Array(s);
    for(var i=0;i<s;i++) map[i]=new Array(s);
    return map;
}
function getSession(){
    return new Promise((resolve,reject)=>{
        firebase.auth().onAuthStateChanged(u=>{
            if(u){
                resolve(u);
            }else{
                location.href="index.html";
            }
        });
    });
}
