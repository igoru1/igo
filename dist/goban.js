class Goban{
    /**
     * elem : CANVAS要素のDOMオブジェクト
     * w : 碁盤の位置辺の長さ
     */
    constructor(elem,w,s){
        this.c=elem;
        this.c.width=w;
        this.c.height=w+2*w/s;
        this.d=document.createElement("canvas");
        this.d.width=w;
        this.d.height=w+2*w/s;
        this.w=w/s;
        this.h=w/s;
        this.s=s;
        this.r=w/(s*2+2)-4;
        this.ox=this.w/2;
        this.oy=this.h/2+w*2/s;
        this.ctx=this.c.getContext("2d");
        this.ctx_=this.d.getContext("2d");
        this.ctx_.textAlign="center";
        this.ctx_.font="30px";
        this.mode=0;
        this.answer=[];
        this.clicked=false;
        this.waiting=null;
        const waiting_b=new Image();
        waiting_b.src="img/b_wait.png";
        const waiting_w=new Image();
        waiting_w.src="img/w_wait.png";
        const waiting_b2=new Image();
        waiting_b2.src="img/b_wait2.png";
        const waiting_w2=new Image();
        waiting_w2.src="img/w_wait2.png";
        this.waiting_img=[
            [waiting_b,waiting_b2],
            [waiting_w,waiting_w2]
        ];
        this.init();
        this.hor_image=new Image();
        this.hor_image.src="img/hor.png";
        this.ver_image=new Image();
        this.ver_image.src="img/ver.png";
        this.dot_image=new Image();
        this.dot_image.src="img/dot.png";
        this.waiting_dot=new Image();
        this.waiting_dot.src="img/dot2.png";
        var bnormal=new Image();
        bnormal.src="img/black.png";
        var bopen=new Image();
        bopen.src="img/black2.png";
        var bakubi=new Image();
        bakubi.src="img/black3.png";
        this.b_stones=[
            bnormal,
            bopen,
            bakubi
        ];
        var wnormal=new Image();
        wnormal.src="img/white.png";
        var wopen=new Image();
        wopen.src="img/white2.png";
        var wakubi=new Image();
        wakubi.src="img/white3.png";
        this.w_stones=[
            wnormal,
            wopen,
            wakubi
        ];
        this.c.onclick=e=>{
            const r=e.target.getBoundingClientRect();
            const x=Math.round(this.s*((e.clientX-r.left)/(r.width-this.w/2)));
            const y=Math.round(this.s*((e.clientY-r.top)/(r.height-this.h/2)));
            if(this.mode==0){
                if(this.ban[y][x]==undefined){
                    this.ban[y][x]=new Stone(0,x,y,this.b_stones);
                }else{
                    this.ban[y][x].drop();
                }
            }else if(this.mode==1){
                this.answer.push(new Stone(this.answer.length%2,x,y,this.answer.length%2==0?this.b_stones:this.w_stones));
            }   
        }
        var ua = navigator.userAgent;
        if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0||ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
            this.c.ontouchstart=f=>{
                var c=f.changedTouches[0]
                var e={
                    target:c.target,
                    clientX:c.clientX,
                    clientY:c.clientY-this.w*3
                };
                this.mousedown(e);
            }
            this.c.ontouchmove=f=>{
                var c=f.changedTouches[0]
                var e={
                    target:c.target,
                    clientX:c.clientX,
                    clientY:c.clientY-this.w*3
                };
                this.mousemove(e);
            }
            this.c.ontouchend=f=>{
                var c=f.changedTouches[0]
                var e={
                    target:c.target,
                    clientX:c.clientX,
                    clientY:c.clientY-this.w*3
                };
                this.mouseup(e);
            }
        }else{
            this.c.onmousedown=e=>this.mousedown(e);
            this.c.onmousemove=e=>this.mousemove(e);
            this.c.onmouseup=e=>this.mouseup(e);
        }
        this.c.ondblclick=e=>{
            const r=e.target.getBoundingClientRect();
            const x=Math.round(this.s*((e.clientX-r.left)/r.width));
            const y=Math.round(this.s*((e.clientY-r.top)/r.height));
            if(this.mode==0){
                this.ban[y][x]=new Stone(1,x,y,this.w_stones);
            }
        }
        this.render();
        this.waiting_state=0;
        setInterval(()=>{
            this.waiting_state=1-this.waiting_state;
        },500);
    }
    mouseup(e){
        const r=e.target.getBoundingClientRect();
        var x=Math.round(this.s*((e.clientX-this.w/2-r.left)/r.width));
        var y=Math.round(this.s*((e.clientY-r.top)/r.width));
        if(x<0||y<0||x>=this.s||y>=this.s){
            this.dropWaiting();
            return false;
        }
        this.clicked=false;
        if(this.mode==2){
            const ev=new Event("clickpoint");
            ev.point=[x,y];
            if("onclickpoint" in this) this.onclickpoint(ev);
            this.dropWaiting();
        }
    }
    mousedown(e){
        const r=e.target.getBoundingClientRect();
        var x=Math.round(this.s*((e.clientX-this.w/2-r.left)/r.width));
        var y=Math.round(this.s*((e.clientY-r.top)/r.width));
        if(x<0||y<0||x>=this.s||y>=this.s){
            this.dropWaiting();
            return false;
        }
        this.clicked=true;
        if(this.mode==2){
            const ev=new Event("setpoint");
            ev.point=[x,y];
            if("onsetpoint" in this) this.onsetpoint(ev);
        }
    }
    mousemove(e){
        const r=e.target.getBoundingClientRect();
        var x=Math.round(this.s*((e.clientX-this.w/2-r.left)/r.width));
        var y=Math.round(this.s*((e.clientY-r.top)/r.width));
        if(x<0||y<0||x>=this.s||y>=this.s){
            this.dropWaiting();
            return false;
        }
        if(this.clicked==false) return false;
        if(this.mode==2){
            const ev=new Event("movepoint");
            ev.point=[x,y];
            if("onmovepoint" in this) this.onmovepoint(ev);
        }
    }
    setWaiting(color,x,y){
        this.waiting=[color,x,y];    
    }
    dropWaiting(){
        this.waiting=null;
    }
    /**
     * 碁盤、解答データ、状態を初期化
     */
    init(){
        if(this.mode!=2)this.mode=0;
        this.ban=[];
        this.answer=[];
        for(let i=0;i<this.s;i++){
            this.ban.push(new Array(this.s));
        }
    }
    /**
     * 状態を変更
     */
    chmode(mode){
        this.mode=mode;
    }
    /**
     * レンダリング
     */
    render(){
        this.ctx.clearRect(0,0,this.c.width,this.c.height);
        this.ctx_.clearRect(0,0,this.c.width,this.c.height);
        for(let i=0;i<this.s;i++){
            for(let j=0;j<this.s;j++){
                
                if(i!=this.s-1){
                    this.ctx.drawImage(this.ver_image,0,0,7,64,this.ox+j*this.w-this.w*7/128,this.oy+i*this.h,this.w*7/64,this.h);
                }
                if(j!=this.s-1){
                    this.ctx.drawImage(this.hor_image,0,0,64,7,this.ox+j*this.w,this.oy+i*this.h-this.h*7/128,this.w,this.h*7/64);
                }
                if(j==(this.s-1)/2&&i==(this.s-1)/2){
                    this.ctx.drawImage(this.dot_image,0,0,21,21,this.ox+this.w*(this.s-1)/2-this.w*21/128,this.oy+this.h*(this.s-1)/2-this.h*21/128,this.w*21/64,this.h*21/64);
                }
                if(this.s>9){
                    if(Math.floor(this.s/2)-Math.abs(j-Math.floor(this.s/2))==3 && Math.floor(this.s/2)-Math.abs(i-Math.floor(this.s/2))==3){
                        this.ctx.drawImage(this.dot_image,0,0,21,21,this.ox+this.w*j-this.w*21/128,this.oy+this.h*i-this.h*21/128,this.w*21/64,this.h*21/64);
                    }
                    
                }
                if(this.s>13){
                    if((Math.floor(this.s/2)-Math.abs(j-Math.floor(this.s/2))==3 && Math.floor(this.s/2)==i)||(Math.floor(this.s/2)-Math.abs(i-Math.floor(this.s/2))==3 && Math.floor(this.s/2)==j)){
                        this.ctx.drawImage(this.dot_image,0,0,21,21,this.ox+this.w*j-this.w*21/128,this.oy+this.h*i-this.h*21/128,this.w*21/64,this.h*21/64);
                    }

                }
                if(this.ban[i][j]!=undefined&&this.ban[i][j]!=null){
                    if(this.ban[i][j].color==0){
                        this.ctx.drawImage(this.ban[i][j].image(),0,0,400,400,this.ox+j*this.w-this.w/2,this.oy+i*this.h-this.h/2,this.w,this.h)
                    }else{
                        this.ctx.drawImage(this.ban[i][j].image(),0,0,400,400,this.ox+j*this.w-this.w/2,this.oy+i*this.h-this.h/2,this.w,this.h)
                    }
                }
            }
        }
        for(let k=0;k<this.answer.length;k++){
            const point=this.answer[k].point();
            const x=point[0];
            const y=point[1];
            if(k%2==1){
                this.ctx.drawImage(this.answer[k].image(),0,0,400,400,this.ox+j*this.w-this.w/2,this.oy+i*this.h-this.h/2,this.w,this.h)
            }else{
                this.ctx.drawImage(this.answer[k].image(),0,0,400,400,this.ox+j*this.w-this.w/2,this.oy+i*this.h-this.h/2,this.w,this.h)
            }
            if(this.answer[k].color==0){
                this.ctx_.fillStyle="#000000";
            }else{
                this.ctx_.fillStyle="#ffffff";
                this.ctx_.strokeStyle="#000000";
            }
            this.ctx_.fillText(k,this.ox+x*this.w,this.oy+y*this.h);
        }
        if(this.waiting!=null){
            this.ctx.drawImage(this.waiting_img[this.waiting[0]][this.waiting_state],0,0,492,492,this.ox+this.waiting[1]*this.w-this.w/2,this.oy+(this.waiting[2]-2)*this.h,this.w*1.2,this.h*1.2);
            if(!this.ban[this.waiting[2]][this.waiting[1]]){
                this.ctx.drawImage(this.waiting_dot,0,0,21,21,this.ox+this.waiting[1]*this.w-this.w*21/128,this.oy+this.waiting[2]*this.h-this.h*21/128,this.w*21/64,this.h*21/64);
            }
        }
        this.ctx.drawImage(this.d,0,0,this.c.width,this.c.height)

        this.renderUpdate();
    }
    /**
     * レンダリングのトリガー関数
     */
    renderUpdate(){
        requestAnimationFrame(()=>{
            this.render();
        });
    }
    /*
     * 入力済みデータの出力を行う関数
     * return {
     *     cond: 盤面の2d配列,
     *     answer: 解答データの2d配列
     * }
     */
    getData(){
        return {
            "cond":this.getBoard(),
            "answer":this.getAnswer()
        }
    }
    /**
     * 解答データの最後を消す
     */
    back(){
        this.answer.pop();
    }
    getAnswer(){
        var answer=[];
        this.answer.forEach((e)=>{
            answer.push(e.point());
        });
        return answer;
    }
    /**
     * 盤面データ取得
     */
    getBoard(){
        var ban=new Array(this.s);
        for(var i=0;i<this.s;i++){
            ban[i]=new Array(this.s);
        }
        for(var i=0;i<this.s;i++){
            for(var j=0;j<this.s;j++){
                if(this.ban[i][j]!=null&&this.ban[i][j]!=undefined) ban[i][j]=this.ban[i][j].color;
            }
        }
        return ban;
    }
    /**
     * 盤面データ代入
     */
    setBoard(ban){
        for(var i=0;i<this.s;i++){
            for(var j=0;j<this.s;j++){
                if(this.ban[i][j]==undefined||this.ban[i][j]==null){
                    if(ban[i][j]!=null&&ban[i][j]!=undefined){
                        this.ban[i][j]=new Stone(ban[i][j],j,i,ban[i][j]==0?this.b_stones:this.w_stones);
                    }
                }else if(ban[i][j]==undefined||ban[i][j]==null){
                    this.ban[i][j].drop();
                    delete this.ban[i][j];
                }
            }
        }
    }
}        
class Stone{
    constructor(color,x,y,imgs){
        this.color=color;
        this.x=x;
        this.y=y;
        this.images=imgs;
        this.state=0;
        this.pid=setInterval(()=>{
            if(this.state!=0){
                this.state=0;
            }else{
                this.state=Math.floor(Math.random()*2);
            }
        },1000);
    }
    color(){
        return this.color;
    }
    image(){
        return this.images[this.state];
    }
    point(){
        return [this.x,this.y];
    }
    drop(){
        clearInterval(this.pid);
        delete this;
    }
}
