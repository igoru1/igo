/**
 *
 * {
 *      points:[
 *          [x,y],
 *          ...
 *      ],
 *      lines:[
 *          [n,m],
 *          ...
 *      ],
 *      vertex:[
 *          n,
 *          ...
 *      ]
 * }
 */
class Line{
    constructor(points=[],lines=[],vertex=[],edge_t=[],edge_b=[],edge_r=[],edge_l=[],color=null){
        this.points=points;
        this.lines=lines;
        this.vertex=vertex;
        this.edge_t=edge_t;
        this.edge_b=edge_b;
        this.edge_l=edge_l;
        this.edge_r=edge_r;
        this.color=color;
    }
}
class Group{
    constructor(points=[],lines=[],edge_t=[],edge_b=[],edge_r=[],edge_l=[],color=null,closed=false){
        this.points=points;
        this.lines=lines;
        this.edge_t=edge_t;
        this.edge_b=edge_b;
        this.edge_r=edge_r;
        this.edge_l=edge_l;
        this.color=color;
        this.closed=false;
        this.children=[];
        this.parent=null;
        this.point=0;
    }
    addChild(group){
        this.children.push(group);
        group.parent=this;
    }
    isParent(group,s){
        return this.points.some(p=>{
            return isIn(group,p[0],p[1],s);
        });
    }
    isRoot(){
        return this.parent==null;
    }
    countEyes(){
        if(!this.closed) return 0;
        var eular=1;
        if(this.edge_t.length+this.edge_l.length+this.edge_r.length+this.edge_b.length==2) eular=0;
        return this.points.length-this.lines.length+eular;
    }
    getPoints(){
        var p=[0,0];
        if(this.parent!=null&&this.point<3&&this.color!=this.parent.color&&this.countEyes()<2){
            p[this.parent.color]+=this.point+this.points.length;
            p[this.color]-=this.points.length;
        }else{
            p[this.color]+=this.point;
        }
        console.log(this.countEyes(),this);
        this.children.forEach(g=>{
            var q=g.getPoints();
            p[0]+=q[0];
            p[1]+=q[1];
        });
        return p;
    }
}
function getLine(checkmap,ban,prev,x,y,line=new Line()){
    var color=ban[y][x];
    checkmap[y][x]=1;
    var direc_count=0;
    var direc=[0,0];
    if(prev!=null){
        direc=[
            x-prev[0],
            y-prev[1]
        ];
    }
    var rightangle=0;
    if(color==null||color==undefined) return line;
    if(prev==null){
        line.points.push([x,y]);
    }
    if(x>0&&ban[y][x-1]==color&&checkmap[y][x-1]!=1){
        direc_count++;
        if(isRightangle(direc,[-1,0])==0) rightangle++;
        line.points.push([x-1,y]);
        line.lines.push(
            [
                [x,y],
                [x-1,y]
            ]
        );
        line=getLine(checkmap,ban,[x,y],x-1,y,line);
    }
    if(x<ban.length-1&&ban[y][x+1]==color&&checkmap[y][x+1]!=1){
        direc_count++;
        if(isRightangle(direc,[1,0])==0) rightangle++;
        line.points.push([x+1,y]);
        line.lines.push(
            [
                [x,y],
                [x+1,y]
            ]
        );
        line=getLine(checkmap,ban,[x,y],x+1,y,line);
    }
    if(y>0&&ban[y-1][x]==color&&checkmap[y-1][x]!=1){
        direc_count++;
        if(isRightangle(direc,[0,-1])==0) rightangle++;
        line.points.push([x,y-1]);
        line.lines.push(
            [
                [x,y],
                [x,y-1]
            ]
        );
        line=getLine(checkmap,ban,[x,y],x,y-1,line);
    }
    if(y<ban.length-1&&ban[y+1][x]==color&&checkmap[y+1][x]!=1){
        direc_count++;
        if(isRightangle(direc,[0,1])==0) rightangle++;
        line.points.push([x,y+1]);
        line.lines.push(
            [
                [x,y],
                [x,y+1]
            ]
        );
        line=getLine(checkmap,ban,[x,y],x,y+1,line);
    }
    if(rightangle==1||prev==null||direc_count==0){
        line.vertex.push([x,y]);
    }
    line.color=ban[y][x];
    if(x==0) line.edge_l.push(y);
    if(x==ban.length-1) line.edge_r.push(y);
    if(y==0) line.edge_t.push(x);
    if(y==ban.length-1) line.edge_b.push(x);
    return line;
}

function getGroup(lines){
    var groups=[];
    var checked=new Array(lines.length);
    for(var i=0;i<lines.length;i++){
        if(checked[i]==1) continue;
        groups.push(getGrouped(checked,lines,i));
    }
    return groups;
}
function getGrouped(checker,lines,i){
    var group=new Group(
        lines[i].points,
        lines[i].lines,
        lines[i].edge_t,
        lines[i].edge_b,
        lines[i].edge_r,
        lines[i].edge_l,
        lines[i].color,
        false
    );
    checker[i]=1;
    lines[i].vertex.forEach(v=>{
        for(var j=0;j<lines.length;j++){
            if(j==i) continue;
            if(lines[j].color!=lines[i].color) continue;
            for(var k=0;k<lines[j].vertex.length;k++){
                var w=lines[j].vertex[k];
                if(((v[0]-w[0])**2+(v[1]-w[1])**2)==2){
                    if(checker[j]==1){
                        group.closed=true;
                        continue;
                    }
                    var grouped=getGrouped(checker,lines,j);
                    group.lines=group.lines.concat(grouped.lines);
                    group.lines.push([v,w]);
                    group.points=group.points.concat(grouped.points);
                    group.edge_l=group.edge_l.concat(grouped.edge_l);
                    group.edge_r=group.edge_r.concat(grouped.edge_r);
                    group.edge_t=group.edge_t.concat(grouped.edge_t);
                    group.edge_b=group.edge_b.concat(grouped.edge_b);
                    group.closed=grouped.closed;
                    break;
                }
            }
        }
    });
    if(Math.sign(group.edge_l.length)+Math.sign(group.edge_r.length)+Math.sign(group.edge_t.length)+Math.sign(group.edge_b.length)>1) group.closed=true;
    if(group.closed==false){
        group.edge_l.sort().some((e,i,a)=>{
            if((i!=a.length&&a.indexOf(e+1)!=-1)||(i!=0&&a.indexOf(e-1)!=-1)){
                group.closed=true;
            }
        });
        group.edge_r.sort().some((e,i,a)=>{
            if((i!=a.length&&a.indexOf(e+1)!=-1)||(i!=0&&a.indexOf(e-1)!=-1)){
                group.closed=true;
            }
        });
        group.edge_t.sort().some((e,i,a)=>{
            if((i!=a.length&&a.indexOf(e+1)!=-1)||(i!=0&&a.indexOf(e-1)!=-1)){
                group.closed=true;
            }
        });
        group.edge_b.sort().some((e,i,a)=>{
            if((i!=a.length&&a.indexOf(e+1)!=-1)||(i!=0&&a.indexOf(e-1)!=-1)){
                group.closed=true;
            }
        });
    }
    return group;
}
function isRightangle(a,b){
    return a[0]*b[1]+a[1]*b[0]==0;
}
function isIn(group,x,y,s){
    var points=group.points.concat();
    if(group.edge_r.length>0) points.push([s+1,y]);
    if(group.edge_l.length>0) points.push([-1,y]);
    var c=0;
    var flg=false;
    for(var i=x+1;i<s+2;i++){
        flg=points.some(e=>{
            if(e[0]==i&&e[1]==y){
                if(!flg){
                    c++;
                }
                return true;
            }
        });
    }
    var result1=c%2==1;
    var c_=0;
    var flg_=false;
    for(var i=x-1;i>-2;i--){
        flg_=points.some(e=>{
            if(e[0]==i&&e[1]==y){
                if(!flg_){
                    c_++;
                }
                return true;
            }
        });
    }
    var result2=c_%2==1;
    return result1&&result2;
}
function getGroupTree(groups,s){
    var roots=[];
    for(var i=0;i<groups.length;i++){
        for(var j=0;j<groups.length;j++){
            if(i==j) continue;
            if(groups[i].isParent(groups[j],s)){
                groups[j].addChild(groups[i]);
                if(roots.indexOf(groups[i])!=-1) roots.splice(roots.indexOf(groups[i]),1,groups[j]);
                if(groups[j].isRoot()&&roots.indexOf(groups[i])==-1) roots.push(groups[j]);
            }
        }
    }
    groups.some(g=>{
        if(g.parent==null&g.children.length==0) roots.push(g);
    });
    return roots;
}
