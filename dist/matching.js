window.onload=async function(){
    dialogPolyfill.registerDialog(document.querySelector("#setting"));
    var current_user=await getSession();
    if(!current_user){
        firebase.auth().signInWithRedirect(new firebase.auth.TwitterAuthProvider());
    }
    const db=firebase.database();
    const rooms=db.ref("rooms");
    const users=db.ref("users");
    document.querySelector("#name").innerHTML="ようこそ、"+current_user.displayName+"さん";
    const container=document.querySelector("#container");
    const user_list=document.querySelector("#user_list");
    document.querySelector("#add").onclick=()=>document.querySelector("#setting").showModal();
    document.querySelector("#cancel").onclick=()=>document.querySelector("#setting").close();
    document.querySelector("#go").onclick=()=>{
        var formdata=document.querySelector("#detail");
        var ref=rooms.push();
        ref.set({
            offer:null,
            answer:null,
            host:current_user.uid,
            color:Number(formdata.color.value),
            size:Number(formdata.size.value)
        });
        location.href="game.html?room_id="+ref.key;
    }
    rooms.on("child_added",e=>{
        var data=e.val();
        users.child(data.host).once("value",f=>{
            var prof=f.val();
            const list=document.createElement("div");
            const link=document.createElement("a");
            const icon=document.createElement("img");
            icon.src=prof.icon;
            const detail=document.createElement("div");
            const title=document.createElement("h4");
            title.innerHTML=prof.displayName;
            //const rate=document.createElement("span");
            //rate.classList.add("rate");
            //rate.innerHTML=prof.rate;
            //title.append(rate);
            const turn=document.createElement("img");
            turn.classList.add("turn");
            turn.src=data.color==0?"img/black.png":"img/white.png";
            const size=document.createElement("span");
            size.classList.add("turn");
            size.innerHTML=data.size+"路盤";
            const header=document.createElement("div");
            header.append(turn);
            header.append(size);
            header.classList.add("head");
            detail.append(header);
            detail.append(title);
            link.append(icon);
            link.append(detail);
            link.href="game.html?room_id="+e.key;
            link.id=data.key;
            list.append(link);
            user_list.insertBefore(list,user_list.querySelector("div"));
        });
    });
    rooms.on("child_removed",e=>{
        container.removeChild(document.querySelector("#"+e.val().key));
    });
}

function getSession(){
    return new Promise((resolve,reject)=>{
        firebase.auth().onAuthStateChanged(e=>{
            resolve(e);
        })
    });
}
